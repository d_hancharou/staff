package sevice.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
@Entity
@Table(name = "PROJECTS")
public class Project implements Serializable {

    public static final String ID = "id";

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Group> groups;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Utilization> utilizations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Group> getGroups() {
        if (this.groups == null) {
            this.groups = new ArrayList<>();
        }
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Utilization> getUtilizations() {
        if (this.utilizations == null) {
            this.utilizations = new ArrayList<>();
        }
        return utilizations;
    }

    public void setUtilizations(List<Utilization> utilizations) {
        this.utilizations = utilizations;
    }
}
