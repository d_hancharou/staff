package sevice.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author dgoncharov
 */
@Entity
@Table(name = "UTILIZATIONS")
public class Utilization implements Serializable {

    public static final String EMPLOYEE =  "employee";
    public static final String PROJECT =  "project";

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "RATE", nullable = false)
    private Integer rate;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEES_ID", referencedColumnName = "ID", nullable = false)
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "PROJECTS_ID", referencedColumnName = "ID", nullable = false)
    private Project project;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
