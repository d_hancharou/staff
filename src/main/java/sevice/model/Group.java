package sevice.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author dgoncharov
 *
 */
@Entity
@Table(name = "GROUPS")
public class Group implements Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "CN")
    private String cn;

    @ManyToOne
    @JoinColumn(name = "PROJECTS_ID", referencedColumnName = "ID", nullable = false)
    private Project project;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
