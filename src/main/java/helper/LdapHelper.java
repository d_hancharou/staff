package helper;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;

/**
 * @author dgoncharov
 */
public class LdapHelper {

    public static final String DISPLAY_NAME = "displayName";
    public static final String DISTINGUISHED_NAME = "distinguishedName";
    public static final String MEMBER_OF = "memberOf";
    public static final String MEMBER = "member";
    public static final String MAIL = "mail";
    public static final String OFFICE_NAME = "physicalDeliveryOfficeName";
    public static final String MOBILE = "mobile";
    public static final String TITLE = "title";
    public static final String CN_LOWER_CASE = "cn";


    public static LdapContext getLdapContext(String login, String password) throws NamingException {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://10.99.55.5:389");
        //env.put(Context.PROVIDER_URL, "ldap://10.99.54.5:389");
        //env.put(Context.PROVIDER_URL, "ldap://artgroup.local");
        env.put(Context.SECURITY_AUTHENTICATION,"simple");
        env.put(Context.SECURITY_PRINCIPAL, login);
        env.put(Context.SECURITY_CREDENTIALS, password);
        return new InitialLdapContext(env, null);
    }

    public static void append(StringBuilder filter, String attr, String name) {
        filter.append("(").append(attr).append("=").append(name).append(")");
    }
}
