package api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
public class EmployeeDto implements Serializable {

    private String id;

    private String firstName;

    private String secondName;

    private String middleName;

    private Integer age;

    private Gender gender;

    private List<UtilizationDto> utilizations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<UtilizationDto> getUtilizations() {
        if (this.utilizations == null) {
            this.utilizations = new ArrayList<>();
        }
        return utilizations;
    }

    public void setUtilizations(List<UtilizationDto> utilizations) {
        this.utilizations = utilizations;
    }
}
