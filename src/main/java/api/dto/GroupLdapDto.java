package api.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
public class GroupLdapDto {

    private String cn;

    private List<UserLdapDto> users;

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public List<UserLdapDto> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<UserLdapDto> users) {
        this.users = users;
    }
}
