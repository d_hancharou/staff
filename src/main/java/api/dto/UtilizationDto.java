package api.dto;

import java.io.Serializable;

/**
 * Created by dgoncharov on 23.05.2017.
 */
public class UtilizationDto implements Serializable {

    private String id;

    private Integer rate;

    private EmployeeDto employee;

    private ProjectDto project;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public EmployeeDto getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDto employee) {
        this.employee = employee;
    }

    public ProjectDto getProject() {
        return project;
    }

    public void setProject(ProjectDto project) {
        this.project = project;
    }
}
