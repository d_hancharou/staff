package api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
public class UserLdapDto implements Serializable {

    private String physicalDeliveryOfficeName;

    private String displayName;

    private String mail;

    private String mobile;

    private String title;

    private List<String> memberOf;

    public String getPhysicalDeliveryOfficeName() {
        return physicalDeliveryOfficeName;
    }

    public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) {
        this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getMemberOf() {
        if (this.memberOf == null) {
            this.memberOf = new ArrayList<>();
        }
        return this.memberOf;
    }

    public void setMemberOf(List<String> memberOf) {
        this.memberOf = memberOf;
    }
}
