package api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
public class GroupDto implements Serializable {

    private String id;

    private String cn;

    private ProjectDto project;

    private List<UserLdapDto> users;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public ProjectDto getProject() {
        return project;
    }

    public void setProject(ProjectDto project) {
        this.project = project;
    }

    public List<UserLdapDto> getUsers() {
        if (users ==  null) {
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<UserLdapDto> users) {
        this.users = users;
    }
}
