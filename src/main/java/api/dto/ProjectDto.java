package api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dgoncharov
 */
public class ProjectDto implements Serializable {

    private String id;

    private String name;

    private List<UtilizationDto> utilizations;

    private List<GroupDto> groups;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UtilizationDto> getUtilizations() {
        if (this.utilizations == null) {
            this.utilizations = new ArrayList<>();
        }
        return utilizations;
    }

    public void setUtilizations(List<UtilizationDto> utilizations) {
        this.utilizations = utilizations;
    }

    public List<GroupDto> getGroups() {
        if (this.groups == null) {
            this.groups = new ArrayList<>();
        }
        return groups;
    }

    public void setGroups(List<GroupDto> groups) {
        this.groups = groups;
    }
}
