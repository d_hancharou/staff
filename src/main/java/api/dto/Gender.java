package api.dto;

/**
 * Created by dgoncharov on 30.05.2017.
 */
public enum Gender {

    MALE,
    FEMALE
}
