package rest;

import api.dto.GroupLdapDto;
import api.dto.GroupSearch;
import api.dto.UserLdapDto;

import javax.ejb.Stateless;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.SortControl;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static helper.LdapHelper.*;

/**
 * @author dgoncharov
 */
@Path("/groups")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Stateless
public class GroupRest {

    @Path("/find/groups")
    @POST
    public Response findUsers(GroupSearch groupSearch) {
        List<GroupLdapDto> response = new ArrayList<>();
        try {
            LdapContext ctx = getLdapContext(groupSearch.getLogin(), groupSearch.getPassword());
            NamingEnumeration<SearchResult> results = searchGroups(ctx, groupSearch, false);
            while (results.hasMoreElements()) {
                response.add(mapResult(ctx, results.next(), false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(response).build();
    }

    @Path("/group/info")
    @POST
    public Response getGroupInfo(GroupSearch groupSearch) {
        GroupLdapDto response = null;
        try {
            LdapContext ctx = getLdapContext(groupSearch.getLogin(), groupSearch.getPassword());
            NamingEnumeration<SearchResult> results = searchGroups(ctx, groupSearch, true);
            if (results.hasMoreElements()) {
                response = mapResult(ctx, results.next(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(response).build();
    }

    private GroupLdapDto mapResult(LdapContext ctx, SearchResult resultGroup, boolean getUsers) throws NamingException, IOException {
        GroupLdapDto groupLdap = new GroupLdapDto();
        Attributes attrs = resultGroup.getAttributes();
        if (attrs.get(CN_LOWER_CASE) != null) {
            groupLdap.setCn((String) attrs.get(CN_LOWER_CASE).get());
        }
        if (getUsers) {
            if (attrs.get(MEMBER) != null) {
                NamingEnumeration<?> members = attrs.get(MEMBER).getAll();
                while (members.hasMoreElements()) {
                    NamingEnumeration<SearchResult> searchResultUser = searchUser(ctx, (String) members.next());
                    if (searchResultUser.hasMoreElements()) {
                        SearchResult searchResult = searchResultUser.next();
                        Attributes attributes = searchResult.getAttributes();
                        if (attributes.get(DISPLAY_NAME) != null) {
                            UserLdapDto ldapDto = new UserLdapDto();
                            ldapDto.setDisplayName((String) attributes.get(DISPLAY_NAME).get());
                            groupLdap.getUsers().add(ldapDto);
                        }
                    }

                }
            }
        }
        return groupLdap;
    }

    private NamingEnumeration<SearchResult> searchGroups(LdapContext ctx, GroupSearch groupSearch, boolean getUsers) throws NamingException, IOException {
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchBase = "DC=ARTGROUP,DC=local";
        List<String> returningAttributes = new ArrayList<>(Arrays.asList(CN_LOWER_CASE));
        if (getUsers) {
            returningAttributes.add(MEMBER);
        }
        ctls.setReturningAttributes(returningAttributes.toArray(new String[0]));
        StringBuilder filter = new StringBuilder();
        filter.append("(&(objectCategory=group)");
        if (groupSearch.getCn() != null && !groupSearch.getCn().isEmpty()) {
            append(filter, CN_LOWER_CASE, "*" + groupSearch.getCn() + "*");
        }
        filter.append(")");
        ctx.setRequestControls(new Control[]{
                new SortControl(new String[]{CN_LOWER_CASE}, Control.CRITICAL)
        });
        return ctx.search(searchBase, filter.toString(), ctls);
    }

    private NamingEnumeration<SearchResult> searchUser(LdapContext ctx, String distinguishedName) throws NamingException, IOException {
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchBase = "DC=ARTGROUP,DC=local";
        List<String> returningAttributes = new ArrayList<>(Arrays.asList(DISPLAY_NAME, DISTINGUISHED_NAME));
        ctls.setReturningAttributes(returningAttributes.toArray(new String[0]));
        StringBuilder filter = new StringBuilder();
        filter.append("(&(objectClass=user)(objectCategory=person)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(physicalDeliveryOfficeName=DND))");
        append(filter, DISTINGUISHED_NAME, distinguishedName.replaceAll("\\\\,", "\\\\5C,"));
        filter.append(")");
        ctx.setRequestControls(new Control[]{
                new SortControl(new String[]{DISPLAY_NAME}, Control.CRITICAL)
        });
        return ctx.search(searchBase, filter.toString(), ctls);
    }

}
