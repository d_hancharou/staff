package rest;

import api.dto.UserLdapDto;
import api.dto.UserSearch;

import javax.ejb.Stateless;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;

import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.SortControl;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

import static helper.LdapHelper.*;

/**
 * @author dgoncharov
 */
@Path("/users")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Stateless
public class UserRest {

    @Path("/find/users")
    @POST
    public Response findUsers(UserSearch userSearch) {
        List<UserLdapDto> response = new ArrayList<>();
        try {
            LdapContext ctx = getLdapContext(userSearch.getLogin(), userSearch.getPassword());
            NamingEnumeration<SearchResult> results = searchUsers(ctx, userSearch, false);
            while (results.hasMoreElements()) {
                response.add(mapResult(ctx, results.next(), false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(response).build();
    }

    @Path("/get/user")
    @POST
    public Response getUser(UserSearch userSearch) {
        UserLdapDto response = null;
        try {
            LdapContext ctx = getLdapContext(userSearch.getLogin(), userSearch.getPassword());
            NamingEnumeration<SearchResult> results = searchUsers(ctx, userSearch, true);
            if (results.hasMoreElements()) {
                response = mapResult(ctx, results.next(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(response).build();
    }

    private UserLdapDto mapResult(LdapContext ctx, SearchResult resultUser, boolean fullInfo) throws NamingException, IOException {
        UserLdapDto userDto = new UserLdapDto();
        Attributes attrs = resultUser.getAttributes();
        if (attrs.get(DISPLAY_NAME) != null) {
            userDto.setDisplayName((String) attrs.get(DISPLAY_NAME).get());
        }
        if (attrs.get(MAIL) != null) {
            userDto.setMail((String) attrs.get(MAIL).get());
        }
        if (attrs.get(TITLE) != null) {
            userDto.setTitle((String) attrs.get(TITLE).get());
        }
        if (fullInfo) {
            if (attrs.get(OFFICE_NAME) != null) {
                userDto.setPhysicalDeliveryOfficeName((String) attrs.get(OFFICE_NAME).get());
            }
            if (attrs.get(MOBILE) != null) {
                userDto.setMobile((String) attrs.get(MOBILE).get());
            }

            if (attrs.get(CN_LOWER_CASE) != null) {
                NamingEnumeration<SearchResult> results = searchGroupOfUser(ctx, (String) attrs.get(CN_LOWER_CASE).get());
                while (results.hasMoreElements()) {
                    SearchResult resultGroupOfUser = results.next();
                    Attributes attrsGroup = resultGroupOfUser.getAttributes();
                    if (attrsGroup.get(DISPLAY_NAME) != null) {
                        userDto.getMemberOf().add((String) attrsGroup.get(DISPLAY_NAME).get());
                    }
                }
            }
        }
        return userDto;
    }

    private NamingEnumeration<SearchResult> searchUsers(LdapContext ctx, UserSearch userSearch, boolean fullInfo) throws NamingException, IOException {
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchBase = "DC=ARTGROUP,DC=local";
        List<String> returningAttributes = new ArrayList<>(Arrays.asList(DISPLAY_NAME, MAIL, TITLE, CN_LOWER_CASE, "range=3-6"));
        if (fullInfo) {
            returningAttributes.addAll(Arrays.asList(OFFICE_NAME, MOBILE));
        }
        ctls.setReturningAttributes(returningAttributes.toArray(new String[0]));
        StringBuilder filter = new StringBuilder();
        filter.append("(&(objectClass=user)(objectCategory=person)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(physicalDeliveryOfficeName=DND))");
        if (userSearch.getOfficeName() != null && !userSearch.getOfficeName().isEmpty() && !"ALL".equals(userSearch.getOfficeName())) {
            append(filter, OFFICE_NAME, userSearch.getOfficeName());
        }
        if (userSearch.getMail() != null && !userSearch.getMail().isEmpty()) {
            append(filter, MAIL, userSearch.getMail());
        }
        if (userSearch.getName() != null && !userSearch.getName().isEmpty()) {
            append(filter, DISPLAY_NAME, "*" + userSearch.getName() + "*");
        }
        if (userSearch.getTitle() != null && !userSearch.getTitle().isEmpty()) {
            append(filter, TITLE, "*" + userSearch.getTitle() + "*");
        }
        filter.append(")");
        ctx.setRequestControls(new Control[]{
                new SortControl(new String[]{DISPLAY_NAME}, Control.CRITICAL)
        });
        return ctx.search(searchBase, filter.toString(), ctls);
    }

    private NamingEnumeration<SearchResult> searchGroupOfUser(LdapContext ctx, String cn) throws NamingException, IOException {
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchBase = "DC=ARTGROUP,DC=local";
        ctls.setReturningAttributes(new String[]{DISPLAY_NAME});
        StringBuilder filter = new StringBuilder();
        filter.append("(&(objectCategory=group)(member=CN=");
        filter.append(cn.replaceAll(",", "\\\\5C,"));
        filter.append(", CN=Users,DC=ARTGROUP,DC=local))");
        ctx.setRequestControls(new Control[]{
                new SortControl(new String[]{DISPLAY_NAME}, Control.CRITICAL)
        });
        return ctx.search(searchBase, filter.toString(), ctls);
    }

}
