package rest;

import api.dto.EmployeeDto;
import api.dto.ProjectDto;
import api.dto.UtilizationDto;
import org.hibernate.Session;

import sevice.model.Employee;
import sevice.model.Project;
import sevice.model.Utilization;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author dgoncharov
 */

@Path("/employees")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Stateless
public class EmployeeRest {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("pu");
    private Session session;

    @PostConstruct
    private void getEntityManager() {
        session = factory.createEntityManager().unwrap(Session.class);
    }

    @Path("/find")
    @POST
    public Response find(Employee employee) {
        session.getTransaction().begin();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = cb.createQuery(Employee.class);
        criteriaQuery.select(criteriaQuery.from(Employee.class));
        List<Employee> resultList = session.createQuery(criteriaQuery).getResultList();
        return Response.ok(mapEmployees(resultList)).build();
    }

    @Path("/{id}")
    @GET
    public Response getEmployee(@PathParam("id") String id) {
        session.getTransaction().begin();
        EmployeeDto employeeDto = mapEmployeeDto(session.get(Employee.class, id));
        session.close();
        return Response.ok(employeeDto).build();
    }

    @Path("/")
    @POST
    public Response create(Employee request) {
        session.getTransaction().begin();
        request.setId(UUID.randomUUID().toString());
        session.save(request);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/")
    @PUT
    public Response update(Employee request) {
        session.getTransaction().begin();
        Employee employee = session.get(Employee.class, request.getId());
        employee.setFirstName(request.getFirstName());
        employee.setSecondName(request.getSecondName());
        employee.setMiddleName(request.getMiddleName());
        employee.setAge(request.getAge());
        employee.setGender(request.getGender());
        session.update(employee);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/{id}")
    @DELETE
    public Response remove(@PathParam("id") String id) {
        session.getTransaction().begin();
        Employee employee = session.get(Employee.class, id);
        session.delete(employee);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/find/utilization")
    @GET
    public Response findUtilization(@QueryParam("employeeId") String employeeId, @QueryParam("projectId") String projectId) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Utilization> query = cb.createQuery(Utilization.class);
        Root<Utilization> from = query.from(Utilization.class);
        query.select(from);
        Join<Utilization, Employee> employee = from.join(Utilization.EMPLOYEE);
        Join<Utilization, Project> project = from.join(Utilization.PROJECT);
        query.where(cb.and(cb.equal(employee.get(Employee.ID), employeeId),
                cb.equal(project.get(Project.ID), projectId)));
        List<Utilization> utilizations = session.createQuery(query).getResultList();
        Utilization utilization = null;
        if (utilizations != null && !utilizations.isEmpty()) {
            utilization = utilizations.iterator().next();
        }
        return Response.ok(mapUtilization(utilization)).build();
    }

    @Path("/add/utilization")
    @PUT
    public Response addUtilization(Utilization utilization) {
        session.getTransaction().begin();
        utilization.setId(UUID.randomUUID().toString());
        session.save(utilization);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/update/utilization")
    @POST
    public Response updateUtilization(Utilization request) {
        session.getTransaction().begin();
        Utilization utilization = session.get(Utilization.class, request.getId());
        utilization.setRate(request.getRate());
        session.update(utilization);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/delete/utilization/{id}")
    @DELETE
    public Response deleteUtilization(@PathParam("id") String id) {
        session.getTransaction().begin();
        Utilization utilization = session.get(Utilization.class, id);
        session.delete(utilization);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    private UtilizationDto mapUtilization(Utilization utilization) {
        UtilizationDto dto = null;
        if (utilization != null) {
            dto = new UtilizationDto();
            dto.setId(utilization.getId());
            dto.setRate(utilization.getRate());
            dto.setProject(new ProjectDto());
            dto.getProject().setId(utilization.getProject().getId());
            dto.getProject().setName(utilization.getProject().getName());
            dto.setEmployee(new EmployeeDto());
            dto.getEmployee().setId(utilization.getEmployee().getId());
            dto.getEmployee().setFirstName(utilization.getEmployee().getFirstName());
            dto.getEmployee().setSecondName(utilization.getEmployee().getSecondName());
            dto.getEmployee().setMiddleName(utilization.getEmployee().getMiddleName());
            dto.getEmployee().setAge(utilization.getEmployee().getAge());
            dto.getEmployee().setGender(utilization.getEmployee().getGender());
        }
        return dto;
    }

    private List<EmployeeDto> mapEmployees(List<Employee> employees) {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        if (employees != null) {
            for (Employee employee : employees) {
                employeeDtos.add(mapEmployeeDto(employee));
            }
        }
        return employeeDtos;
    }

    private EmployeeDto mapEmployeeDto(Employee employee) {
        EmployeeDto employeeDto = null;
        if (employee != null) {
            employeeDto = new EmployeeDto();
            employeeDto.setId(employee.getId());
            employeeDto.setFirstName(employee.getFirstName());
            employeeDto.setSecondName(employee.getSecondName());
            employeeDto.setMiddleName(employee.getMiddleName());
            employeeDto.setAge(employee.getAge());
            employeeDto.setGender(employee.getGender());
            if (employee.getUtilizations() != null) {
                for (Utilization utilization : employee.getUtilizations()) {
                    UtilizationDto utilizationDto = new UtilizationDto();
                    utilizationDto.setId(utilization.getId());
                    utilizationDto.setRate(utilization.getRate());
                    ProjectDto projectDto = new ProjectDto();
                    projectDto.setId(utilization.getProject().getId());
                    projectDto.setName(utilization.getProject().getName());
                    utilizationDto.setProject(projectDto);
                    employeeDto.getUtilizations().add(utilizationDto);
                }
            }
        }
        return employeeDto;
    }
}
