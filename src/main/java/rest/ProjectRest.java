package rest;

import api.dto.EmployeeDto;
import api.dto.GroupDto;
import api.dto.ProjectDto;
import api.dto.UtilizationDto;
import org.hibernate.Session;
import sevice.model.Group;
import sevice.model.Project;
import sevice.model.Utilization;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author dgoncharov
 */

@Path("/projects")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Stateless
public class ProjectRest {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("pu");
    private Session session;

    @PostConstruct
    private void getEntityManager() {
        session = factory.createEntityManager().unwrap(Session.class);
    }

    @Path("/find")
    @POST
    public Response find(Project project) {
        session.getTransaction().begin();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery = cb.createQuery(Project.class);
        criteriaQuery.select(criteriaQuery.from(Project.class));
        List<Project> resultList = session.createQuery(criteriaQuery).getResultList();
        return Response.ok(mapProjects(resultList)).build();
    }

    @Path("/{id}")
    @GET
    public Response getProject(@PathParam("id") String id) {
        session.getTransaction().begin();
        ProjectDto projectDto = mapProjectDto(session.get(Project.class, id));
        return Response.ok(projectDto).build();
    }


    @Path("/")
    @POST
    public Response create(Project project) {
        session.getTransaction().begin();
        project.setId(UUID.randomUUID().toString());
        session.save(project);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/")
    @PUT
    public Response update(Project request) {
        session.getTransaction().begin();
        Project project = session.get(Project.class, request.getId());
        project.setName(request.getName());
        project.setGroups(request.getGroups());
        session.update(project);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/{id}")
    @DELETE
    public Response remove(@PathParam("id") String id) {
        session.getTransaction().begin();
        Project project = session.get(Project.class, id);
        session.delete(project);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    @Path("/add/group")
    @POST
    public Response addGroup(GroupDto groupDto) {
        session.getTransaction().begin();
        Group group = new Group();
        group.setId(UUID.randomUUID().toString());
        group.setCn(groupDto.getCn());
        Project project = new Project();
        project.setId(groupDto.getProject().getId());
        group.setProject(project);
        session.save(group);
        session.getTransaction().commit();
        return Response.ok().build();
    }

    private Project getProjectById(String id) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery = cb.createQuery(Project.class);
        Root<Project> from = criteriaQuery.from(Project.class);
        criteriaQuery.select(from);
        criteriaQuery.where(cb.equal(from.get(Project.ID), id));
        return session.createQuery(criteriaQuery).getSingleResult();
    }

    private List<ProjectDto> mapProjects(List<Project> projects) {
        List<ProjectDto> projectDtos = new ArrayList<>();
        if (projects != null) {
            for (Project project : projects) {
                projectDtos.add(mapProjectDto(project));
            }
        }
        return projectDtos;
    }

    private ProjectDto mapProjectDto(Project project) {
        ProjectDto projectDto = null;
        if (project != null) {
            projectDto = new ProjectDto();
            projectDto.setId(project.getId());
            projectDto.setName(project.getName());
            for (Utilization utilization : project.getUtilizations()) {
                UtilizationDto utilizationDto = new UtilizationDto();
                utilizationDto.setId(utilization.getId());
                utilizationDto.setRate(utilization.getRate());
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.setId(utilization.getEmployee().getId());
                employeeDto.setFirstName(utilization.getEmployee().getFirstName());
                employeeDto.setSecondName(utilization.getEmployee().getSecondName());
                employeeDto.setMiddleName(utilization.getEmployee().getMiddleName());
                employeeDto.setAge(utilization.getEmployee().getAge());
                utilizationDto.setEmployee(employeeDto);
                projectDto.getUtilizations().add(utilizationDto);
            }
            for (Group group : project.getGroups()) {
                GroupDto groupDto = new GroupDto();
                groupDto.setId(group.getId());
                groupDto.setCn(group.getCn());
                projectDto.getGroups().add(groupDto);
            }
        }
        return projectDto;
    }
}
