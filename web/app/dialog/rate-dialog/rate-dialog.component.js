"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var utilization_1 = require("../../model/utilization");
var forms_1 = require("@angular/forms");
var RateDialogComponent = (function () {
    function RateDialogComponent(fb, dialogRef, data) {
        this.fb = fb;
        this.dialogRef = dialogRef;
        this.data = data;
        // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
        this.formErrors = {
            "rate": ""
        };
        // Объект с сообщениями ошибок
        this.validationMessages = {
            "rate": {
                "required": "Обязательное поле.",
                "minlength": "Значение должно быть не менее 1го символа.",
                "maxlength": "Значение не должно быть больше 3х символов.",
                "pattern": "Значение должно быть целым числом."
            }
        };
        if (data.utilization) {
            this.utilization = data.utilization;
        }
        else {
            this.utilization = new utilization_1.Utilization();
        }
        this.projectName = data.projectName;
        this.employeeName = data.employeeName;
        this.dialogText = "Вы действительно хотите " + (this.utilization ? "отредактировать данные " : "добавить ")
            + this.employeeName + " на " + this.projectName + "?";
    }
    RateDialogComponent.prototype.ngOnInit = function () {
        this.buildFrom();
        if (!this.utilization) {
            this.utilization = new utilization_1.Utilization();
        }
        this.rateForm.patchValue(this.utilization);
    };
    RateDialogComponent.prototype.onSubmit = function (rateFrom) {
        this.dialogRef.close(rateFrom.value.rate);
    };
    RateDialogComponent.prototype.buildFrom = function () {
        var _this = this;
        this.rateForm = this.fb.group({
            "rate": [this.utilization.rate, [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(1),
                    forms_1.Validators.maxLength(3),
                    forms_1.Validators.pattern("\\d+")
                ]]
        });
        this.rateForm.valueChanges
            .subscribe(function (data) { return _this.onValueChange(data); });
        this.onValueChange();
    };
    RateDialogComponent.prototype.onValueChange = function (data) {
        if (!this.rateForm)
            return;
        var form = this.rateForm;
        for (var field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var message = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    };
    return RateDialogComponent;
}());
RateDialogComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "rate-dialog",
        templateUrl: "rate-dialog.component.html",
        styleUrls: ["../../../node_modules/bootstrap/dist/css/bootstrap.css",
            "rate-dialog.component.css"]
    }),
    __param(2, core_1.Inject(material_1.MD_DIALOG_DATA)),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        material_1.MdDialogRef, Object])
], RateDialogComponent);
exports.RateDialogComponent = RateDialogComponent;
//# sourceMappingURL=rate-dialog.component.js.map