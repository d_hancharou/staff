import {Component, Inject, OnInit} from "@angular/core";
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {Utilization} from "../../model/utilization";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";



@Component({
    moduleId: module.id,
    selector: "rate-dialog",
    templateUrl: "rate-dialog.component.html",
    styleUrls: ["../../../node_modules/bootstrap/dist/css/bootstrap.css",
        "rate-dialog.component.css"]
})
export class RateDialogComponent implements OnInit {

    utilization: Utilization;
    projectName: string;
    employeeName: string;
    dialogText: string;

    rateForm: FormGroup;
    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors = {
        "rate": ""
    };
    // Объект с сообщениями ошибок
    validationMessages = {
        "rate": {
            "required": "Обязательное поле.",
            "minlength": "Значение должно быть не менее 1го символа.",
            "maxlength": "Значение не должно быть больше 3х символов.",
            "pattern": "Значение должно быть целым числом."
        }
    };

    constructor(private fb: FormBuilder,
                public dialogRef: MdDialogRef<RateDialogComponent>,
                @Inject(MD_DIALOG_DATA) public data: any) {
        if (data.utilization) {
            this.utilization = data.utilization;
        } else {
            this.utilization = new Utilization();
        }
        this.projectName = data.projectName;
        this.employeeName = data.employeeName;
        this.dialogText = "Вы действительно хотите " + (this.utilization ? "отредактировать данные " : "добавить ")
            + this.employeeName + " на " + this.projectName + "?";
    }

    ngOnInit(): void {
        this.buildFrom();
        if (!this.utilization) {
            this.utilization = new Utilization();
        }
        this.rateForm.patchValue(this.utilization);
    }

    onSubmit(rateFrom: FormGroup) {
        this.dialogRef.close(rateFrom.value.rate)
    }

    buildFrom() {
        this.rateForm = this.fb.group({
            "rate" : [this.utilization.rate, [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(3),
                Validators.pattern("\\d+")
            ]]
        });
        this.rateForm.valueChanges
            .subscribe(data => this.onValueChange(data));
        this.onValueChange();
    }

    onValueChange(data?: any) {
        if (!this.rateForm) return;
        let form = this.rateForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

}
