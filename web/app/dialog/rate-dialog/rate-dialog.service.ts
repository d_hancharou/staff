import {Injectable} from "@angular/core";
import {MdDialog, MdDialogRef} from "@angular/material";
import {Observable} from "rxjs/Observable";
import {RateDialogComponent} from "./rate-dialog.component";
import {Utilization} from "../../model/utilization";

@Injectable()
export class RateDialogService {

    utilization: Utilization;

    constructor(private dialog: MdDialog) {}

    public confirm(utilization: Utilization, projectName: string, employeeName): Observable<number> {
        this.utilization = utilization;
        let dialogRef: MdDialogRef<RateDialogComponent>;
        dialogRef = this.dialog.open(RateDialogComponent, {
            data: {
                "utilization" : utilization,
                "projectName" : projectName,
                "employeeName" : employeeName
            }
        });
        return dialogRef.afterClosed();
    }

}