"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var employee_create_update_component_1 = require("./employee/create-update/employee-create-update.component");
var staff_component_1 = require("./staff/staff.component");
var project_create_update_component_1 = require("./project/create-update/project-create-update.component");
var view_project_component_1 = require("./project/view-project/view-project.component");
var user_component_1 = require("./art-staff/user/user-info/user.component");
var user_search_component_1 = require("./art-staff/user/user-search/user-search.component");
var main_page_component_1 = require("./art-staff/main-page.component");
var project_group_component_1 = require("./art-staff/project/project-group/project-group.component");
var group_info_component_1 = require("./art-staff/group/group-info/group-info.component");
var page_not_found_component_1 = require("./404/page-not-found.component");
exports.routes = [
    {
        path: "",
        redirectTo: "main",
        pathMatch: "full"
    },
    { path: "staff", component: staff_component_1.StaffComponent },
    { path: "employees/edit/:id", component: employee_create_update_component_1.EmployeeCreateUpdateComponent },
    { path: "employees/create", component: employee_create_update_component_1.EmployeeCreateUpdateComponent },
    { path: "projects/create", component: project_create_update_component_1.ProjectCreateUpdateComponent },
    { path: "projects/edit/:id", component: project_create_update_component_1.ProjectCreateUpdateComponent },
    { path: "project/:id", component: view_project_component_1.ViewProjectComponent },
    { path: "user", component: user_search_component_1.UserSearchComponent },
    { path: "user/info", component: user_component_1.UserComponent },
    { path: "main", component: main_page_component_1.MainPageComponent },
    { path: "project/groups/:id", component: project_group_component_1.ProjectGroupComponent },
    { path: "group/info", component: group_info_component_1.GroupInfoComponent },
    { path: "**", component: page_not_found_component_1.PageNotFoundComponent }
];
//# sourceMappingURL=app.router.js.map