"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var employee_service_1 = require("../service/employee.service");
var project_service_1 = require("../service/project.service");
var utilization_1 = require("../model/utilization");
var confirm_dialog_service_1 = require("../dialog/confirm-dialog/confirm-dialog-service");
var rate_dialog_service_1 = require("../dialog/rate-dialog/rate-dialog.service");
var StaffComponent = (function () {
    function StaffComponent(router, employeeService, projectService, confirmDialogService, rateDialogService) {
        this.router = router;
        this.employeeService = employeeService;
        this.projectService = projectService;
        this.confirmDialogService = confirmDialogService;
        this.rateDialogService = rateDialogService;
    }
    StaffComponent.prototype.ngOnInit = function () {
        this.findEmployees();
        this.getProjects();
    };
    StaffComponent.prototype.findEmployees = function () {
        var _this = this;
        this.employeeService.find().subscribe(function (employees) { return _this.employees = employees; }, function (error) { return _this.errorMessage = error; });
    };
    StaffComponent.prototype.createEmployee = function () {
        this.router.navigate(["employees", "create"]);
    };
    StaffComponent.prototype.editEmpoyee = function (id) {
        this.router.navigate(["employees", "edit", id]);
    };
    StaffComponent.prototype.deleteEmployee = function (employee) {
        var _this = this;
        this.confirmDialogService.confirm("Удаление пользователя", "Действительно хотите удалить " + employee.firstName + " " + employee.secondName + "?")
            .subscribe(function (deleteEmployee) {
            if (deleteEmployee) {
                _this.employeeService.deleteEmployee(employee.id)
                    .subscribe(function () { return _this.findEmployees(); }, function (error) { return _this.errorMessage = error; });
            }
        });
    };
    StaffComponent.prototype.createProject = function () {
        this.router.navigate(["projects", "create"]);
    };
    StaffComponent.prototype.editProject = function (id) {
        this.router.navigate(["projects", "edit", id]);
    };
    StaffComponent.prototype.showProject = function (id) {
        this.router.navigate(["project", id]);
    };
    StaffComponent.prototype.deleteProject = function (project) {
        var _this = this;
        this.confirmDialogService.confirm("Удаление проекта", "Действительно хотите удалить проект " + project.name + "?")
            .subscribe(function (deleteProject) {
            if (deleteProject) {
                _this.projectService.remove(project.id).then(function () { return _this.getProjects(); }, function (error) { return _this.errorMessage = error; });
            }
        });
    };
    StaffComponent.prototype.onDragStart = function (event, employee) {
        var data = JSON.stringify(employee);
        event.dataTransfer.setData('employee', data);
    };
    StaffComponent.prototype.allowDrop = function (event) {
        event.preventDefault();
    };
    StaffComponent.prototype.onDrop = function (event, projectIn) {
        this.utilization = new utilization_1.Utilization();
        var employeeIn = JSON.parse(event.dataTransfer.getData('employee'));
        this.utilization.employee = employeeIn;
        this.utilization.project = projectIn;
        this.openDialogRate(employeeIn, projectIn);
        event.preventDefault();
    };
    StaffComponent.prototype.getProjects = function () {
        var _this = this;
        this.projectService.findProjects().then(function (projects) { return _this.projects = projects; }, function (error) { return _this.errorMessage = error; });
    };
    StaffComponent.prototype.openDialogRate = function (employee, project) {
        var _this = this;
        this.employeeService.findUtilization(employee.id, project.id).subscribe(function (utilization) {
            var employeeName = _this.utilization.employee.firstName + " " + _this.utilization.employee.secondName;
            _this.rateDialogService.confirm(utilization, _this.utilization.project.name, employeeName).subscribe(function (rate) {
                if (rate) {
                    _this.utilization.rate = rate;
                    if (utilization) {
                        _this.utilization.id = utilization.id;
                        _this.employeeService.updateUtilization(_this.utilization).subscribe(function () { }, function (error) { return _this.errorMessage; });
                    }
                    else {
                        _this.employeeService.addUtilization(_this.utilization).subscribe(function () { }, function (error) { return _this.errorMessage; });
                    }
                }
            });
        }, function (error) { return _this.errorMessage = error; });
    };
    return StaffComponent;
}());
StaffComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "staff",
        templateUrl: "staff.component.html",
        styleUrls: ["staff.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        employee_service_1.EmployeeService,
        project_service_1.ProjectService,
        confirm_dialog_service_1.ConfirmDialogService,
        rate_dialog_service_1.RateDialogService])
], StaffComponent);
exports.StaffComponent = StaffComponent;
//# sourceMappingURL=staff.component.js.map