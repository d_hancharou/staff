import {Router} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import {Project} from "../model/project";
import {Employee} from "../model/employee";
import {EmployeeService} from "../service/employee.service";
import {ProjectService} from "../service/project.service";
import {Utilization} from "../model/utilization";
import {ConfirmDialogService} from "../dialog/confirm-dialog/confirm-dialog-service";
import {RateDialogService} from "../dialog/rate-dialog/rate-dialog.service";

@Component({
    moduleId: module.id,
    selector: "staff",
    templateUrl: "staff.component.html",
    styleUrls: ["staff.component.css"]
})
export class StaffComponent implements OnInit {

    employees: Array<Employee>;
    projects: Array<Project>;
    errorMessage: string;

    utilization: Utilization;

    constructor(private router: Router,
                private employeeService: EmployeeService,
                private projectService: ProjectService,
                private confirmDialogService: ConfirmDialogService,
                private rateDialogService: RateDialogService) {
    }

    ngOnInit(): void {
        this.findEmployees();
        this.getProjects();
    }

    private findEmployees() {
        this.employeeService.find().subscribe(
            employees => this.employees = employees,
            error => this.errorMessage = error
        );
    }

    public createEmployee() {
        this.router.navigate(["employees", "create"]);
    }

    public editEmpoyee(id: string) {
        this.router.navigate(["employees", "edit", id]);
    }

    public deleteEmployee(employee: Employee) {
        this.confirmDialogService.confirm(
            "Удаление пользователя",
            "Действительно хотите удалить " + employee.firstName + " " + employee.secondName + "?")
            .subscribe(deleteEmployee => {
                if (deleteEmployee) {
                    this.employeeService.deleteEmployee(employee.id)
                        .subscribe(
                            () => this.findEmployees(),
                            error => this.errorMessage = error
                        );
                }
            });
    }

    public createProject() {
        this.router.navigate(["projects", "create"]);
    }

    public editProject(id: string) {
        this.router.navigate(["projects", "edit", id]);
    }

    public showProject(id: string) {
        this.router.navigate(["project", id]);
    }

    public deleteProject(project: Project) {
        this.confirmDialogService.confirm(
            "Удаление проекта",
            "Действительно хотите удалить проект " + project.name + "?")
            .subscribe(deleteProject => {
                if (deleteProject) {
                    this.projectService.remove(project.id).then(
                        () => this.getProjects(),
                        error => this.errorMessage = error
                    )
                }
            });
    }

    public onDragStart(event, employee: Employee) {
        let data = JSON.stringify(employee);
        event.dataTransfer.setData('employee', data);
    }

    allowDrop(event) {
        event.preventDefault();
    }

    public onDrop(event, projectIn: Project) {
        this.utilization = new Utilization();
        let employeeIn: Employee = JSON.parse(event.dataTransfer.getData('employee'));
        this.utilization.employee = employeeIn;
        this.utilization.project = projectIn;
        this.openDialogRate(employeeIn, projectIn);
        event.preventDefault();
    }

    private getProjects() {
        this.projectService.findProjects().then(
            projects => this.projects = projects,
            error => this.errorMessage = error
        );
    }

    private openDialogRate(employee: Employee, project: Project) {
        this.employeeService.findUtilization(employee.id, project.id).subscribe(
            utilization => {
                let employeeName = this.utilization.employee.firstName + " " + this.utilization.employee.secondName;
                this.rateDialogService.confirm(utilization, this.utilization.project.name, employeeName).subscribe(rate => {
                    if (rate) {
                        this.utilization.rate = rate;
                        if (utilization) {
                            this.utilization.id = utilization.id;
                            this.employeeService.updateUtilization(this.utilization).subscribe(
                                () => {},
                                error => this.errorMessage
                            );
                        } else {
                            this.employeeService.addUtilization(this.utilization).subscribe(
                                () => {},
                                error => this.errorMessage
                            );
                        }
                    }
                });
            },
            error => this.errorMessage = error
        );
    }

}
