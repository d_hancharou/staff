import {Component, OnInit} from "@angular/core";
import {Group} from "../../../model/group.ldap";
import {PagerService} from "../../../service/pager/pager.service";
import {GroupService} from "../../../service/group.service";
import {Router, RouterStateSnapshot} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: "group-search",
    templateUrl: "group-search.component.html",
    styleUrls: ["group-search.component.css"]
})
export class GroupSearchComponent implements OnInit {

    // pager object
    pager: any = {};
    // paged items
    pagedItems: Group[];

    cn: string;

    groups: Array<Group>;

    errorMessage: string;

    constructor(private router: Router,
                private groupService: GroupService,
                private pagerService: PagerService) {
    }

    ngOnInit(): void {

    }

    findGroups() {
        this.groupService.findGroups(this.cn).subscribe(
            groups => {
                this.groups = groups;
                this.setPage(1);
            },
            error => this.errorMessage = error
        );
    }

    public onDragStart(event, cn: string) {
        let data = JSON.stringify(cn);
        event.dataTransfer.setData('cn', data);
    }

    public setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.groups.length, page);
        // get current page of items
        this.pagedItems = this.groups.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}