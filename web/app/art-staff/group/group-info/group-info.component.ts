import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {Group} from "../../../model/group.ldap";
import {GroupService} from "../../../service/group.service";

@Component({
    moduleId: module.id,
    selector: "group-info",
    templateUrl: "group-info.component.html",
    styleUrls: ["group-info.component.css"]
})
export class GroupInfoComponent implements OnInit  {

    errorMessage: String;

    group: Group;

    constructor(private activatedRoute: ActivatedRoute,
                private groupService: GroupService) {
    }

    ngOnInit(): void {
        this.getGroupInfo();
    }

    private getGroupInfo() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let cn = params["cn"];
            if (cn) {
                this.groupService.getGroupInfo(cn).subscribe(
                    group => this.group = group,
                    error => this.errorMessage = error
                );
            } else {
                this.group = new Group();
            }
        });
    }
}