import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../service/user.service";
import {UserLdap} from "../../../model/user.ldap";
import {Router} from "@angular/router";
import {PagerService} from "../../../service/pager/pager.service";

@Component({
    moduleId: module.id,
    selector: "user-search",
    templateUrl: "user-search.component.html",
    styleUrls: ["user-search.component.css"]
})
export class UserSearchComponent implements OnInit {

    // pager object
    pager: any = {};
    // paged items
    pagedItems: UserLdap[];

    offices: any[] = [
        {name: "Все", value: "ALL"},
        {name: "Витебск", value: "VITEBSK"},
        {name: "Минск", value: "MINSK"},
        {name: "Могилев", value: "Mogilev"},
        {name: "Москва", value: "MOSCOW"},
        {name: "Нижний Новгород", value: "NIZHNY NOVGOROD"},
        {name: "Саратов", value: "SARATOV"}
        ];
    selectedOffice: any;
    searchName: string;
    searchTitle: string;

    users: Array<UserLdap>;

    errorMessage: string;

    constructor(private router: Router,
                private userService: UserService,
                private pagerService: PagerService) {
        this.selectedOffice = this.offices[0].value;
    }

    ngOnInit(): void {
    }

    public findUsers() {
        this.userService.findUsers(this.selectedOffice, this.searchName, this.searchTitle).subscribe(
            users => {
                this.users = users;
                this.setPage(1);
            },
            error => this.errorMessage = error
        );
    }

    public goToPageUser(mail: string) {
        this.router.navigate(["user", "info"], {queryParams : {mail : mail}});
    }

    public setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.users.length, page);
        // get current page of items
        this.pagedItems = this.users.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}