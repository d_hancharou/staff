import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {UserService} from "../../../service/user.service";
import {UserLdap} from "../../../model/user.ldap";

@Component({
    moduleId: module.id,
    selector: "user-ldap",
    templateUrl: "user.component.html"
})
export class UserComponent implements OnInit {

    user: UserLdap;

    errorMessage: string;

    constructor(private activatedRoute: ActivatedRoute,
        private userService: UserService) {
    }

    ngOnInit(): void {
        this.getUser();
    }

    private getUser() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let mail = params["mail"];
            if (mail) {
                this.userService.getUser(mail).subscribe(
                    user => this.user = user,
                    error => this.errorMessage = error
                );
            } else {
                this.user = new UserLdap();
            }
        });
    }
}