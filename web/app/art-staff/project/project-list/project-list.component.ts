import {Component, OnInit} from "@angular/core";
import {Project} from "../../../model/project";
import {Router} from "@angular/router";
import {ProjectService} from "../../../service/project.service";
import {ConfirmDialogService} from "../../../dialog/confirm-dialog/confirm-dialog-service";

@Component({
    moduleId: module.id,
    selector: "project-list",
    templateUrl: "project-list.component.html",
    styleUrls: ["project-list.component.css"]
})
export class ProjectListComponent implements OnInit {
    projects: Array<Project>;
    errorMessage: string;

    constructor(private router: Router,
                private confirmDialogService: ConfirmDialogService,
                private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.findProjects();
    }

    public createProject() {
        this.router.navigate(["projects", "create"]);
    }

    public editProject(id: string) {
        this.router.navigate(["projects", "edit", id]);
    }

    public goToProjectGroup(id: string) {
        this.router.navigate(["project", "groups", id]);
    }

    public deleteProject(project: Project) {
        this.confirmDialogService.confirm(
            "Удаление проекта",
            "Действительно хотите удалить проект " + project.name + "?")
            .subscribe(deleteProject => {
                if (deleteProject) {
                    this.projectService.remove(project.id).then(
                        () => this.findProjects(),
                        error => this.errorMessage = error
                    )
                }
            });
    }

    private findProjects() {
        this.projectService.findProjects().then(
            projects => this.projects = projects,
            error => this.errorMessage = error
        );
    }
}