import {Component} from "@angular/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Project} from "../../../model/project";
import {ProjectService} from "../../../service/project.service";
import {ConfirmDialogService} from "../../../dialog/confirm-dialog/confirm-dialog-service";
import {Group} from "../../../model/group.ldap";
@Component({
    moduleId: module.id,
    selector: "project-group",
    templateUrl: "project-group.component.html",
    styleUrls: ["project-group.component.css"]
})
export class ProjectGroupComponent {

    errorMessage: string;
    project: Project;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private confirmDialogService: ConfirmDialogService,
                private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.getProjectFromRoute();
    }

    public allowDrop(event) {
        event.preventDefault();
    }

    public onDrop(event) {
        let cn: string = JSON.parse(event.dataTransfer.getData('cn'));
        this.confirmDialogService.confirm(
            "Добавить группу",
            "Действительно хотите добавить группу " + cn + " к проекту?")
            .subscribe(addGroup => {
                if (addGroup) {
                    let group: Group = new Group();
                    group.cn = cn;
                    group.project = this.project;
                    this.projectService.addGroup(group)
                        .then(
                            () => this.getProjectFromRoute(),
                            error => this.errorMessage = error
                        );
                }
            });
        event.preventDefault();
    }

    goToGroupInfo(cn: String) {
        this.router.navigate(["group", "info"], {queryParams : {cn : cn}});
    }

    private getProjectFromRoute() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = params["id"];
            this.projectService.getProject(id).then(
                project => {
                    this.project = project;
                },
                error => this.errorMessage = error
            );

        });
    }
}