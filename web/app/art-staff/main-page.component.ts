import {Component, OnInit} from "@angular/core";
import {ProjectService} from "../service/project.service";
import {Project} from "../model/project";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: "main-page",
    templateUrl: "main-page.component.html",
    styleUrls: ["main-page.component.css"]
})
export class MainPageComponent {


}