"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var project_service_1 = require("../../service/project.service");
var project_1 = require("../../model/project");
var ProjectCreateUpdateComponent = (function () {
    function ProjectCreateUpdateComponent(fb, router, activatedRoute, projectService) {
        this.fb = fb;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.projectService = projectService;
        this.project = new project_1.Project();
        // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
        this.formErrors = {
            "name": ""
        };
        // Объект с сообщениями ошибок
        this.validationMessages = {
            "name": {
                "required": "Обязательное поле.",
                "minlength": "Значение должно быть не менее 2х символов.",
                "maxlength": "Значение не должно быть больше 15 символов."
            }
        };
    }
    ProjectCreateUpdateComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getProjectFromRoute();
    };
    ProjectCreateUpdateComponent.prototype.goBack = function () {
        this.router.navigate(["main"]);
    };
    ProjectCreateUpdateComponent.prototype.onSubmit = function (projectForm) {
        var _this = this;
        this.project.name = projectForm.value.name;
        if (this.project.id) {
            this.projectService.update(this.project)
                .then(function () { return _this.goBack(); }, function (error) { return _this.errorMessage = error; });
        }
        else {
            this.projectService.create(this.project)
                .then(function () { return _this.goBack(); }, function (error) { return _this.errorMessage = error; });
        }
    };
    ProjectCreateUpdateComponent.prototype.getProjectFromRoute = function () {
        var _this = this;
        this.activatedRoute.params.forEach(function (params) {
            var id = params["id"];
            if (id) {
                _this.projectService.getProject(id).then(function (project) {
                    _this.project = project;
                    _this.projectForm.patchValue(_this.project);
                }, function (error) { return _this.errorMessage = error; });
            }
            else {
                _this.project = new project_1.Project();
                _this.projectForm.patchValue(_this.project);
            }
        });
    };
    ProjectCreateUpdateComponent.prototype.buildForm = function () {
        var _this = this;
        this.projectForm = this.fb.group({
            "name": [this.project.name, [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(2),
                    forms_1.Validators.maxLength(15)
                ]]
        });
        this.projectForm.valueChanges
            .subscribe(function (data) { return _this.onValueChange(data); });
        this.onValueChange();
    };
    ProjectCreateUpdateComponent.prototype.onValueChange = function (data) {
        if (!this.projectForm)
            return;
        var form = this.projectForm;
        for (var field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var message = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    };
    return ProjectCreateUpdateComponent;
}());
ProjectCreateUpdateComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "project-create-update",
        templateUrl: "project-create-update.component.html",
        styleUrls: ["project-create-update.component.css",
            "../../../node_modules/bootstrap/dist/css/bootstrap.css"]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        router_1.Router,
        router_1.ActivatedRoute,
        project_service_1.ProjectService])
], ProjectCreateUpdateComponent);
exports.ProjectCreateUpdateComponent = ProjectCreateUpdateComponent;
//# sourceMappingURL=project-create-update.component.js.map