import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {ProjectService} from "../../service/project.service";
import {Project} from "../../model/project";

@Component({
    moduleId: module.id,
    selector: "project-create-update",
    templateUrl: "project-create-update.component.html",
    styleUrls: ["project-create-update.component.css",
        "../../../node_modules/bootstrap/dist/css/bootstrap.css"]
})
export class ProjectCreateUpdateComponent implements OnInit {

    errorMessage: string;
    projectForm: FormGroup;
    project: Project = new Project();

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors = {
        "name": ""
    };

    // Объект с сообщениями ошибок
    validationMessages = {
        "name": {
            "required": "Обязательное поле.",
            "minlength": "Значение должно быть не менее 2х символов.",
            "maxlength": "Значение не должно быть больше 15 символов."
        }
    };

    constructor(private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.buildForm();
        this.getProjectFromRoute();
    }

    public goBack() {
        this.router.navigate(["main"]);
    }

    public onSubmit(projectForm: FormGroup) {
        this.project.name = projectForm.value.name;

        if (this.project.id) {
            this.projectService.update(this.project)
                .then(
                    () => this.goBack(),
                    error => this.errorMessage = error
                );
        } else {
            this.projectService.create(this.project)
                .then(
                    () => this.goBack(),
                    error => this.errorMessage = error
                );
        }
    }

    private getProjectFromRoute() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = params["id"];
            if (id) {
                this.projectService.getProject(id).then(
                    project => {
                        this.project = project;
                        this.projectForm.patchValue(this.project);
                    },
                    error => this.errorMessage = error
                );
            } else {
                this.project = new Project();
                this.projectForm.patchValue(this.project);
            }
        });
    }

    buildForm() {
        this.projectForm = this.fb.group({
            "name": [this.project.name, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(15)
            ]]
        });

        this.projectForm.valueChanges
            .subscribe(data => this.onValueChange(data));
        this.onValueChange();
    }

    onValueChange(data?: any) {
        if (!this.projectForm) return;
        let form = this.projectForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }
}