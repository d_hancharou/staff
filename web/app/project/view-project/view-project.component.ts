import {Component, Input, OnInit} from "@angular/core";
import {Project} from "../../model/project";
import {ActivatedRoute, Params} from "@angular/router";

import {ProjectService} from "../../service/project.service";
import {EmployeeService} from "../../service/employee.service";
import {Utilization} from "../../model/utilization";
import {ConfirmDialogService} from "../../dialog/confirm-dialog/confirm-dialog-service";

@Component({
    moduleId: module.id,
    selector: "view-project",
    templateUrl: "view-project.component.html",
    styleUrls: []
})
export class ViewProjectComponent implements OnInit {

    errorMessage: string;

    @Input()
    project: Project;

    constructor(private activatedRoute: ActivatedRoute,
                private projectService: ProjectService,
                private employeeService: EmployeeService,
                private confirmDialogService: ConfirmDialogService) {
    }

    ngOnInit(): void {
        this.getProjectFromRoute();
    }

    public updateUtilization(rate: number, utilizationId: string) {
        let utilization = new Utilization();
        utilization.rate = rate;
        utilization.id = utilizationId;
        this.employeeService.updateUtilization(utilization).subscribe(
            null,
            error => this.errorMessage = error
        );
    }

    public deleteUtilization(utiltzationId: string) {
        this.confirmDialogService.confirm(
            "Удаление пользователя с проекта",
            "Вы действительно хотите удалить " + "" + "с проекта?")
            .subscribe(deleteFrom => {
                if (deleteFrom) {
                    this.employeeService.deleteUtilization(utiltzationId).subscribe(
                        () => this.getProjectFromRoute(),
                        error => this.errorMessage = error
                    );
                }
            });
    }

    private getProjectFromRoute() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = params["id"];
            this.projectService.getProject(id).then(
                project => {
                    this.project = project;
                },
                error => this.errorMessage = error
            );

        });
    }
}