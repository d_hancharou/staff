import {Utilization} from "./utilization";
import {Group} from "./group.ldap";

export class Project {

    id: string;
    name: string;
    utilizations: Array<Utilization>;
    groups: Array<Group>;
}