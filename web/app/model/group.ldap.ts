import {Project} from "./project";
import {UserLdap} from "./user.ldap";

export class Group {
    id: string;
    cn: string;
    project: Project;
    users: Array<UserLdap>;
}