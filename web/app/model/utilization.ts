import {Employee} from "./employee";
import {Project} from "./project";

export class Utilization {

    id: string;
    rate: number;
    employee: Employee;
    project: Project;

}