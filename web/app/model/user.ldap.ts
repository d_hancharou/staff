export class UserLdap {
    physicalDeliveryOfficeName: string;
    displayName: string;
    mail: string;
    mobile: string;
    title: string;
    memberOf: Array<string>;
}