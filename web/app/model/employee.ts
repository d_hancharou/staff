import {Utilization} from "./utilization";
import {Gender} from "./gender";

export class Employee {

    id: string;
    firstName: string;
    secondName: string;
    middleName: string;
    age: number;
    gender: Gender;
    utilizations: Array<Utilization>;

}