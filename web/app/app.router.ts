import {Routes} from "@angular/router";
import {EmployeeCreateUpdateComponent} from "./employee/create-update/employee-create-update.component";
import {StaffComponent} from "./staff/staff.component";
import {ProjectCreateUpdateComponent} from "./project/create-update/project-create-update.component";
import {ViewProjectComponent} from "./project/view-project/view-project.component";
import {UserComponent} from "./art-staff/user/user-info/user.component";
import {UserSearchComponent} from "./art-staff/user/user-search/user-search.component";
import {MainPageComponent} from "./art-staff/main-page.component";
import {ProjectGroupComponent} from "./art-staff/project/project-group/project-group.component";
import {GroupInfoComponent} from "./art-staff/group/group-info/group-info.component";
import {PageNotFoundComponent} from "./404/page-not-found.component";

export const routes: Routes = [
    {
        path: "",
        redirectTo: "main",
        pathMatch: "full"
    },
    {path: "staff", component: StaffComponent},
    {path: "employees/edit/:id", component: EmployeeCreateUpdateComponent},
    {path: "employees/create", component: EmployeeCreateUpdateComponent},
    {path: "projects/create", component: ProjectCreateUpdateComponent},
    {path: "projects/edit/:id", component: ProjectCreateUpdateComponent},
    {path: "project/:id", component: ViewProjectComponent},
    {path: "user", component: UserSearchComponent},
    {path: "user/info", component: UserComponent},
    {path: "main", component: MainPageComponent},
    {path: "project/groups/:id", component: ProjectGroupComponent},
    {path: "group/info", component: GroupInfoComponent},

    {path: "**", component: PageNotFoundComponent}
];