"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var employee_1 = require("../model/employee");
var EmployeeService = (function () {
    function EmployeeService(http) {
        this.http = http;
        this.url = "http://localhost:8080/employee/rest/employees";
    }
    EmployeeService.prototype.getEmployee = function (id) {
        return this.http.get(this.url + "/" + id)
            .map(this.extractEmployee)
            .catch(this.handleError);
    };
    EmployeeService.prototype.find = function () {
        var employees = this.http.post(this.url + "/find", new employee_1.Employee())
            .map(this.extractEmployees)
            .catch(this.handleError);
        return employees;
    };
    EmployeeService.prototype.create = function (employee) {
        return this.http.post(this.url, employee)
            .catch(this.handleError);
    };
    EmployeeService.prototype.update = function (employee) {
        return this.http.put(this.url, employee)
            .catch(this.handleError);
    };
    EmployeeService.prototype.deleteEmployee = function (id) {
        return this.http.delete(this.url + "/" + id)
            .catch(this.handleError);
    };
    EmployeeService.prototype.findUtilization = function (employeeId, projectId) {
        var params = new http_1.URLSearchParams();
        params.set("employeeId", employeeId);
        params.set("projectId", projectId);
        var requestOptions = new http_1.RequestOptions();
        requestOptions.params = params;
        return this.http.get(this.url + "/find/utilization", requestOptions)
            .map(this.extractUtilization)
            .catch(this.handleError);
    };
    EmployeeService.prototype.addUtilization = function (utilization) {
        return this.http.put(this.url + "/add/utilization", utilization)
            .catch(this.handleError);
    };
    EmployeeService.prototype.updateUtilization = function (utilization) {
        return this.http.post(this.url + "/update/utilization", utilization)
            .catch(this.handleError);
    };
    EmployeeService.prototype.deleteUtilization = function (id) {
        return this.http.delete(this.url + "/delete/utilization/" + id)
            .catch(this.handleError);
    };
    EmployeeService.prototype.extractUtilization = function (response) {
        if (response.text()) {
            var res = response.json();
            return res;
        }
        else {
            return null;
        }
    };
    EmployeeService.prototype.extractEmployees = function (response) {
        if (response.text()) {
            var res = response.json();
            var employees = [];
            for (var i = 0; i < res.length; i++) {
                employees.push(res[i]);
            }
            return employees;
        }
        else {
            return null;
        }
    };
    EmployeeService.prototype.extractEmployee = function (response) {
        if (response.text()) {
            var res = response.json();
            var employee = res;
            return employee;
        }
        else {
            return null;
        }
    };
    EmployeeService.prototype.handleError = function (error, caught) {
        var message = "";
        if (error instanceof http_1.Response) {
            var errorData = error.json().error || JSON.stringify(error.json());
            message = error.status + " - " + (error.statusText || '') + " " + errorData;
        }
        else {
            message = error.message ? error.message : error.toString();
        }
        console.error(message);
        return Observable_1.Observable.throw(message);
    };
    return EmployeeService;
}());
EmployeeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EmployeeService);
exports.EmployeeService = EmployeeService;
//# sourceMappingURL=employee.service.js.map