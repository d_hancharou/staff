import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";

import { Observable } from "rxjs/Observable";
import {Project} from "../model/project";
import {Group} from "../model/group.ldap";

@Injectable()
export class ProjectService {

    private url: string = "http://localhost:8080/employee/rest/projects";

    constructor(private http: Http) {}

    public findProjects(): Promise<Project[]> {
        let projects = this.http.post(this.url + "/find", new Project())
            .toPromise()
            .then(this.extractProjects)
            .catch(this.handleError);
        return projects;
    }

    public getProject(id: string): Promise<Project> {
        return this.http.get(this.url + "/" + id)
            .toPromise()
            .then(this.extractProject)
            .catch(this.handleError);
    }

    public create(project: Project): Promise<any> {
        return this.http.post(this.url, project)
            .toPromise()
            .catch(this.handleError);
    }

    public update(project: Project): Promise<any> {
        return this.http.put(this.url, project)
            .toPromise()
            .catch(this.handleError);
    }

    public remove(id: string): Promise<any> {
        return this.http.delete(this.url + "/" + id)
            .toPromise()
            .catch(this.handleError);
    }

    public addGroup(group: Group): Promise<any> {
        return this.http.post(this.url + "/add/group", group)
            .toPromise()
            .catch(this.handleError);
    }

    private extractProjects(response: Response) {
        let res = response.json();
        let projects: Project[] = [];
        for (let i = 0; i < res.length; i++) {
            projects.push(<Project>res[i]);
        }
        return projects;
    }

    private extractProject(response: Response) {
        let res = response.json();
        return <Project> res;
    }

    private handleError(error: any): any {
        let message = "";

        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}