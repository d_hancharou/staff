import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {Group} from "../model/group.ldap";

@Injectable()
export class GroupService {

    private url: string = "http://localhost:8080/employee/rest/groups";

    constructor(private http: Http) {
    }

    public findGroups(cn: string): Observable<Group[]> {
        return this.http.post(this.url + "/find/groups",
            {
                "login" : "",
                "password" : "",
                "cn" : cn
            })
            .map(this.extractGroups)
            .catch(this.handleError);
    }

    public getGroupInfo(cn: string): Observable<Group> {
        return this.http.post(this.url + "/group/info",
            {
                "login" : "",
                "password" : "",
                "cn" : cn
            })
            .map(this.extractGroup)
            .catch(this.handleError);
    }

    private extractGroup(response: Response) {
        if (response.text()) {
            return <Group> response.json();
        } else {
            return null;
        }
    }

    private extractGroups(response: Response) {
        if (response.text()) {
            let res = response.json();
            let group: Group[] = [];
            for (let i = 0; i < res.length; i++) {
                group.push(<Group> res[i]);
            }
            return res;
        } else {
            return null;
        }
    }

    private handleError(error: any, caught: Observable<any>): any {
        let message = "";
        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }
        console.error(message);
        return Observable.throw(message);
    }
}