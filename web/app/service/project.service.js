"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var project_1 = require("../model/project");
var ProjectService = (function () {
    function ProjectService(http) {
        this.http = http;
        this.url = "http://localhost:8080/employee/rest/projects";
    }
    ProjectService.prototype.findProjects = function () {
        var projects = this.http.post(this.url + "/find", new project_1.Project())
            .toPromise()
            .then(this.extractProjects)
            .catch(this.handleError);
        return projects;
    };
    ProjectService.prototype.getProject = function (id) {
        return this.http.get(this.url + "/" + id)
            .toPromise()
            .then(this.extractProject)
            .catch(this.handleError);
    };
    ProjectService.prototype.create = function (project) {
        return this.http.post(this.url, project)
            .toPromise()
            .catch(this.handleError);
    };
    ProjectService.prototype.update = function (project) {
        return this.http.put(this.url, project)
            .toPromise()
            .catch(this.handleError);
    };
    ProjectService.prototype.remove = function (id) {
        return this.http.delete(this.url + "/" + id)
            .toPromise()
            .catch(this.handleError);
    };
    ProjectService.prototype.addGroup = function (group) {
        return this.http.post(this.url + "/add/group", group)
            .toPromise()
            .catch(this.handleError);
    };
    ProjectService.prototype.extractProjects = function (response) {
        var res = response.json();
        var projects = [];
        for (var i = 0; i < res.length; i++) {
            projects.push(res[i]);
        }
        return projects;
    };
    ProjectService.prototype.extractProject = function (response) {
        var res = response.json();
        return res;
    };
    ProjectService.prototype.handleError = function (error) {
        var message = "";
        if (error instanceof http_1.Response) {
            var errorData = error.json().error || JSON.stringify(error.json());
            message = error.status + " - " + (error.statusText || '') + " " + errorData;
        }
        else {
            message = error.message ? error.message : error.toString();
        }
        console.error(message);
        return Observable_1.Observable.throw(message);
    };
    return ProjectService;
}());
ProjectService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ProjectService);
exports.ProjectService = ProjectService;
//# sourceMappingURL=project.service.js.map