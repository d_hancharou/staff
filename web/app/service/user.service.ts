import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {UserLdap} from "../model/user.ldap";

@Injectable()
export class UserService {

    private url: string = "http://localhost:8080/employee/rest/users";

    constructor(private http: Http) {
    }

    public findUsers(officeName: string, name: string, title: string): Observable<UserLdap[]> {
        return this.http.post(this.url + "/find/users",
            {
                "login" : "",
                "password" : "",
                "officeName" : officeName,
                "mail" : null,
                "name" : name,
                "title" : title
            })
            .map(this.extractUsers)
            .catch(this.handleError);
    }

    public getUser(mail: string): Observable<UserLdap> {
        return this.http.post(this.url + "/get/user",
            {
                "login" : "",
                "password" : "",
                "mail" : mail
            })
            .map(this.extractUser)
            .catch(this.handleError);
    }

    private extractUser(response: Response) {
        if (response.text()) {
            return <UserLdap> response.json();
        } else {
            return null;
        }
    }

    private extractUsers(response: Response) {
        if (response.text()) {
            let res = response.json();
            let user: UserLdap[] = [];
            for (let i = 0; i < res.length; i++) {
                user.push(<UserLdap> res[i]);
            }
            return res;
        } else {
            return null;
        }
    }

    private handleError(error: any, caught: Observable<any>): any {
        let message = "";
        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }
        console.error(message);
        return Observable.throw(message);
    }
}