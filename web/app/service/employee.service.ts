import {Injectable} from "@angular/core";
import {Http, RequestOptions, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Employee} from "../model/employee";
import {Utilization} from "../model/utilization";

@Injectable()
export class EmployeeService {

    private url: string = "http://localhost:8080/employee/rest/employees";

    constructor(private http: Http) {

    }

    public getEmployee(id: string): Observable<Employee> {
        return this.http.get(this.url + "/" + id)
            .map(this.extractEmployee)
            .catch(this.handleError);
    }

    public find(): Observable<Employee[]> {
        let employees = this.http.post(this.url + "/find", new Employee())
            .map(this.extractEmployees)
            .catch(this.handleError);
        return employees;
    }

    public create(employee: Employee): Observable<Employee> {
        return this.http.post(this.url, employee)
            .catch(this.handleError);
    }

    public update(employee: Employee): Observable<any> {
        return this.http.put(this.url, employee)
            .catch(this.handleError);
    }

    public deleteEmployee(id: String): Observable<any> {
        return this.http.delete(this.url + "/" + id)
            .catch(this.handleError);
    }

    public findUtilization(employeeId: string, projectId: string): Observable<Utilization> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("employeeId", employeeId);
        params.set("projectId", projectId);
        let requestOptions: RequestOptions = new RequestOptions();
        requestOptions.params = params;
        return this.http.get(this.url + "/find/utilization", requestOptions)
            .map(this.extractUtilization)
            .catch(this.handleError);
    }

    public addUtilization(utilization: Utilization): Observable<any> {
        return this.http.put(this.url + "/add/utilization", utilization)
            .catch(this.handleError);
    }

    public updateUtilization(utilization: Utilization): Observable<any> {
        return this.http.post(this.url + "/update/utilization", utilization)
            .catch(this.handleError);
    }

    public deleteUtilization(id: string) {
        return this.http.delete(this.url + "/delete/utilization/" + id)
            .catch(this.handleError);
    }

    private extractUtilization(response: Response) {
        if (response.text()) {
            let res = response.json();
            return <Utilization> res;
        } else {
            return null;
        }
    }

    private extractEmployees(response: Response) {
        if (response.text()) {
            let res = response.json();
            let employees: Employee[] = [];
            for (let i = 0; i < res.length; i++) {
                employees.push(<Employee> res[i]);
            }
            return employees;
        } else {
            return null;
        }
    }

    private extractEmployee(response: Response) {
        if (response.text()) {
            let res = response.json();
            let employee = <Employee> res;
            return employee;
        } else {
            return null;
        }
    }

    private handleError(error: any, caught: Observable<any>): any {
        let message = "";
        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }
        console.error(message);
        return Observable.throw(message);
    }
}