import {NgModule}from "@angular/core";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {MaterialModule, MdDialogModule, MdNativeDateModule, MdTabsModule} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {HttpModule} from "@angular/http";
import {FlexLayoutModule} from "@angular/flex-layout";

import {AppComponent} from "./app.component";
import {RateDialogComponent} from "./dialog/rate-dialog/rate-dialog.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {routes} from "./app.router";
import {EmployeeCreateUpdateComponent} from "./employee/create-update/employee-create-update.component";
import {StaffComponent} from "./staff/staff.component";
import {EmployeeService} from "./service/employee.service";

import {ProjectService} from "./service/project.service";
import {ProjectCreateUpdateComponent} from "./project/create-update/project-create-update.component";
import {ViewProjectComponent} from "./project/view-project/view-project.component";
import {ConfirmDialogService} from "./dialog/confirm-dialog/confirm-dialog-service";
import {ConfirmDialogComponent} from "./dialog/confirm-dialog/confirm-dialog.component";
import {RateDialogService} from "./dialog/rate-dialog/rate-dialog.service";
import {UserService} from "./service/user.service";
import {UserSearchComponent} from "./art-staff/user/user-search/user-search.component";
import {UserComponent} from "./art-staff/user/user-info/user.component";
import {PagerService} from "./service/pager/pager.service";
import {MainPageComponent} from "./art-staff/main-page.component";
import {ProjectListComponent} from "./art-staff/project/project-list/project-list.component";
import {ProjectGroupComponent} from "./art-staff/project/project-group/project-group.component";
import {GroupSearchComponent} from "./art-staff/group/group-search/group-search.component";
import {GroupService} from "./service/group.service";
import {GroupInfoComponent} from "./art-staff/group/group-info/group-info.component";
import {PageNotFoundComponent} from "./404/page-not-found.component";

@NgModule({
    imports: [HttpModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MdDialogModule,
        MdTabsModule,
        RouterModule.forRoot(routes),
        FlexLayoutModule
    ],
    declarations: [AppComponent, RateDialogComponent, ConfirmDialogComponent, EmployeeCreateUpdateComponent,
        StaffComponent, ProjectCreateUpdateComponent, ViewProjectComponent, UserSearchComponent, UserComponent,
        MainPageComponent, ProjectListComponent, ProjectGroupComponent, GroupSearchComponent, GroupInfoComponent,
        PageNotFoundComponent],
    bootstrap: [AppComponent],
    entryComponents: [RateDialogComponent, ConfirmDialogComponent],
    providers: [EmployeeService, ProjectService, ConfirmDialogService, RateDialogService, UserService, PagerService,
        GroupService]
})
export class AppModule {

}