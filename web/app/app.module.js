"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var material_1 = require("@angular/material");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var flex_layout_1 = require("@angular/flex-layout");
var app_component_1 = require("./app.component");
var rate_dialog_component_1 = require("./dialog/rate-dialog/rate-dialog.component");
var forms_1 = require("@angular/forms");
var app_router_1 = require("./app.router");
var employee_create_update_component_1 = require("./employee/create-update/employee-create-update.component");
var staff_component_1 = require("./staff/staff.component");
var employee_service_1 = require("./service/employee.service");
var project_service_1 = require("./service/project.service");
var project_create_update_component_1 = require("./project/create-update/project-create-update.component");
var view_project_component_1 = require("./project/view-project/view-project.component");
var confirm_dialog_service_1 = require("./dialog/confirm-dialog/confirm-dialog-service");
var confirm_dialog_component_1 = require("./dialog/confirm-dialog/confirm-dialog.component");
var rate_dialog_service_1 = require("./dialog/rate-dialog/rate-dialog.service");
var user_service_1 = require("./service/user.service");
var user_search_component_1 = require("./art-staff/user/user-search/user-search.component");
var user_component_1 = require("./art-staff/user/user-info/user.component");
var pager_service_1 = require("./service/pager/pager.service");
var main_page_component_1 = require("./art-staff/main-page.component");
var project_list_component_1 = require("./art-staff/project/project-list/project-list.component");
var project_group_component_1 = require("./art-staff/project/project-group/project-group.component");
var group_search_component_1 = require("./art-staff/group/group-search/group-search.component");
var group_service_1 = require("./service/group.service");
var group_info_component_1 = require("./art-staff/group/group-info/group-info.component");
var page_not_found_component_1 = require("./404/page-not-found.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [http_1.HttpModule,
            platform_browser_1.BrowserModule,
            animations_1.BrowserAnimationsModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            material_1.MdDialogModule,
            material_1.MdTabsModule,
            router_1.RouterModule.forRoot(app_router_1.routes),
            flex_layout_1.FlexLayoutModule
        ],
        declarations: [app_component_1.AppComponent, rate_dialog_component_1.RateDialogComponent, confirm_dialog_component_1.ConfirmDialogComponent, employee_create_update_component_1.EmployeeCreateUpdateComponent,
            staff_component_1.StaffComponent, project_create_update_component_1.ProjectCreateUpdateComponent, view_project_component_1.ViewProjectComponent, user_search_component_1.UserSearchComponent, user_component_1.UserComponent,
            main_page_component_1.MainPageComponent, project_list_component_1.ProjectListComponent, project_group_component_1.ProjectGroupComponent, group_search_component_1.GroupSearchComponent, group_info_component_1.GroupInfoComponent,
            page_not_found_component_1.PageNotFoundComponent],
        bootstrap: [app_component_1.AppComponent],
        entryComponents: [rate_dialog_component_1.RateDialogComponent, confirm_dialog_component_1.ConfirmDialogComponent],
        providers: [employee_service_1.EmployeeService, project_service_1.ProjectService, confirm_dialog_service_1.ConfirmDialogService, rate_dialog_service_1.RateDialogService, user_service_1.UserService, pager_service_1.PagerService,
            group_service_1.GroupService]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map