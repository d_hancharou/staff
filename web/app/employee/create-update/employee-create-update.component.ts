import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Employee} from "../../model/employee";
import {EmployeeService} from "../../service/employee.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: "employee-create-update",
    templateUrl: "employee-create-update.component.html",
    styleUrls: [
        "../../../node_modules/bootstrap/dist/css/bootstrap.css",
        "employee-create-update.component.css"]
})
export class EmployeeCreateUpdateComponent implements OnInit {

    errorMessage: string;
    employeeForm: FormGroup;
    employee: Employee = new Employee();

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors = {
        "firstName": "",
        "secondName": "",
        "age": "",
        "gender": ""
    };

    // Объект с сообщениями ошибок
    validationMessages = {
        "firstName": {
            "required": "Обязательное поле.",
            "minlength": "Значение должно быть не менее 2х символов.",
            "maxlength": "Значение не должно быть больше 15 символов."
        },
        "secondName": {
            "required": "Обязательное поле.",
            "minlength": "Значение должно быть не менее 2х символов.",
            "maxlength": "Значение не должно быть больше 15 символов."
        },
        "age": {
            "required": "Обязательное поле.",
            "pattern": "Значение должно быть целым числом."
        },
        "gender": {
            "required": "Обязательное поле."
        }
    };

    constructor(private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private employeeService: EmployeeService) {

    }

    ngOnInit() {
        this.buildForm();
        this.getEmployeeFromRoute();
    }

    public goBack() {
        this.router.navigate(["/staff"]);
    }

    public onSubmit(employeeForm: FormGroup) {
        this.employee.firstName = employeeForm.value.firstName;
        this.employee.secondName = employeeForm.value.secondName;
        this.employee.age = employeeForm.value.age;
        this.employee.gender = employeeForm.value.gender;

        if (this.employee.id) {
            this.employeeService.update(this.employee)
                .subscribe(
                    () => this.goBack(),
                    error => this.errorMessage = error
                );
        } else {
            this.employeeService.create(this.employee)
                .subscribe(
                    () => this.goBack(),
                    error => this.errorMessage = error
                );
        }
    }

    private getEmployeeFromRoute() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = params["id"];
            if (id) {
                this.employeeService.getEmployee(id).subscribe(
                    employee => {
                        this.employee = employee;
                        this.employeeForm.patchValue(this.employee);
                    },
                    error => this.errorMessage = error
                );
            } else {
                this.employee = new Employee();
                this.employeeForm.patchValue(this.employee);
            }
        });
    }

    buildForm() {
        this.employeeForm = this.fb.group({
            "firstName": [this.employee.firstName, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(15)
            ]],
            "secondName": [this.employee.secondName, [
                Validators.minLength(2),
                Validators.maxLength(15)
            ]],
            "age": [this.employee.age, [
                Validators.required,
                Validators.pattern("\\d+")
            ]],
            "gender": [this.employee.gender, [
                Validators.required
            ]]
        });

        this.employeeForm.valueChanges
            .subscribe(data => this.onValueChange(data));

        this.onValueChange();
    }

    onValueChange(data?: any) {
        if (!this.employeeForm) return;
        let form = this.employeeForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

}