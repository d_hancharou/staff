"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var employee_1 = require("../../model/employee");
var employee_service_1 = require("../../service/employee.service");
var router_1 = require("@angular/router");
var EmployeeCreateUpdateComponent = (function () {
    function EmployeeCreateUpdateComponent(fb, router, activatedRoute, employeeService) {
        this.fb = fb;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.employeeService = employeeService;
        this.employee = new employee_1.Employee();
        // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
        this.formErrors = {
            "firstName": "",
            "secondName": "",
            "age": "",
            "gender": ""
        };
        // Объект с сообщениями ошибок
        this.validationMessages = {
            "firstName": {
                "required": "Обязательное поле.",
                "minlength": "Значение должно быть не менее 2х символов.",
                "maxlength": "Значение не должно быть больше 15 символов."
            },
            "secondName": {
                "required": "Обязательное поле.",
                "minlength": "Значение должно быть не менее 2х символов.",
                "maxlength": "Значение не должно быть больше 15 символов."
            },
            "age": {
                "required": "Обязательное поле.",
                "pattern": "Значение должно быть целым числом."
            },
            "gender": {
                "required": "Обязательное поле."
            }
        };
    }
    EmployeeCreateUpdateComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getEmployeeFromRoute();
    };
    EmployeeCreateUpdateComponent.prototype.goBack = function () {
        this.router.navigate(["/staff"]);
    };
    EmployeeCreateUpdateComponent.prototype.onSubmit = function (employeeForm) {
        var _this = this;
        this.employee.firstName = employeeForm.value.firstName;
        this.employee.secondName = employeeForm.value.secondName;
        this.employee.age = employeeForm.value.age;
        this.employee.gender = employeeForm.value.gender;
        if (this.employee.id) {
            this.employeeService.update(this.employee)
                .subscribe(function () { return _this.goBack(); }, function (error) { return _this.errorMessage = error; });
        }
        else {
            this.employeeService.create(this.employee)
                .subscribe(function () { return _this.goBack(); }, function (error) { return _this.errorMessage = error; });
        }
    };
    EmployeeCreateUpdateComponent.prototype.getEmployeeFromRoute = function () {
        var _this = this;
        this.activatedRoute.params.forEach(function (params) {
            var id = params["id"];
            if (id) {
                _this.employeeService.getEmployee(id).subscribe(function (employee) {
                    _this.employee = employee;
                    _this.employeeForm.patchValue(_this.employee);
                }, function (error) { return _this.errorMessage = error; });
            }
            else {
                _this.employee = new employee_1.Employee();
                _this.employeeForm.patchValue(_this.employee);
            }
        });
    };
    EmployeeCreateUpdateComponent.prototype.buildForm = function () {
        var _this = this;
        this.employeeForm = this.fb.group({
            "firstName": [this.employee.firstName, [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(2),
                    forms_1.Validators.maxLength(15)
                ]],
            "secondName": [this.employee.secondName, [
                    forms_1.Validators.minLength(2),
                    forms_1.Validators.maxLength(15)
                ]],
            "age": [this.employee.age, [
                    forms_1.Validators.required,
                    forms_1.Validators.pattern("\\d+")
                ]],
            "gender": [this.employee.gender, [
                    forms_1.Validators.required
                ]]
        });
        this.employeeForm.valueChanges
            .subscribe(function (data) { return _this.onValueChange(data); });
        this.onValueChange();
    };
    EmployeeCreateUpdateComponent.prototype.onValueChange = function (data) {
        if (!this.employeeForm)
            return;
        var form = this.employeeForm;
        for (var field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var message = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    };
    return EmployeeCreateUpdateComponent;
}());
EmployeeCreateUpdateComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "employee-create-update",
        templateUrl: "employee-create-update.component.html",
        styleUrls: [
            "../../../node_modules/bootstrap/dist/css/bootstrap.css",
            "employee-create-update.component.css"
        ]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        router_1.Router,
        router_1.ActivatedRoute,
        employee_service_1.EmployeeService])
], EmployeeCreateUpdateComponent);
exports.EmployeeCreateUpdateComponent = EmployeeCreateUpdateComponent;
//# sourceMappingURL=employee-create-update.component.js.map